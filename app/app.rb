require 'json'
require 'sinatra/base'
require 'unirest'
require "sinatra/reloader"
require './utils'

require 'sinatra'
require "sinatra/config_file"
require 'omniauth'
require 'omniauth-fitbit'



class DashboardApp < Sinatra::Application
  register Sinatra::ConfigFile
  register Sinatra::Reloader

  also_reload '/views/'

  config_file './config/dashboard_secrets.yml'

  # set :widgets_path, File.join( settings.root, 'widgets' )
  set :session_secret, '*&(^B234'
  use Rack::Session::Cookie
  use OmniAuth::Strategies::Developer


  extend Utils

  use OmniAuth::Builder do
    provider :fitbit, '227S5R', '93af3586a0a70023a0706efaac323b77', scope: "activity profile"
  end

  configure do
    set :bind, '0.0.0.0'
  end

  get '/' do
    slim :index, :layout => :layout
  end

  def widgets
    # list all dir names in widgets path
    @@widgets_list ||= Dir.glob( File.join( settings.root, 'widgets/*' ) ).select { |f| File.directory? f }.map { |item| File.basename( item ) }
  end

  configure_widgets! DashboardApp

  # start the server if ruby file executed directly
  run! if app_file == $0
end
