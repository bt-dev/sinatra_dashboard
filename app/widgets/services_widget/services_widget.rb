class ServicesWidget < BaseWidget

  def self.resource_route
    '/api/v1/:service'
  end

  def data #GET
    case params['service']
      when 'weather'
        # TODO: implement a lock mechanism per service
        lat = params.include?( 'lat' ) ? params['lat'].to_f : 0
        lng = params.include?( 'lng' ) ? params['lng'].to_f : 0
        lat = 45.5582 if lat == 0
        lng = 11.5419 if lng == 0
        response = Unirest.get "https://simple-weather.p.mashape.com/weather?lat=#{lat}&lng=#{lng}", headers: { 'X-Mashape-Key' => '0BB1Aj3dK2mshHuQ9N2zDj4rs1f5p1WUMnajsnFJzM06s1A2qa', 'Accept' => 'text/plain' }
        if response.code == 200
          {result:response.body}.to_json
        else
          [ 503, 'Service not available' ]
        end
      else
        [ 400, 'Invalid service' ]
      end
  end

end
