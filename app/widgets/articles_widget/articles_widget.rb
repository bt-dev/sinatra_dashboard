class ArticlesWidget < BaseWidget

  def self.resource_route
    "/articles.json"
  end

  def data #GET
    articles = [
      { title: 'aaa', category: 'cat 1', description: 'one desc', dt: '-' },
      { title: 'bbb', category: 'cat 2', description: 'another desc', dt: '-' },
      { title: 'ccc', category: 'cat 1', description: 'last desc', dt: '-' }
    ].to_json
  end

end
