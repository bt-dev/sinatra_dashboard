# Sinatra dashboard

> A dynamic dashboard

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload & rebuild (localhost:8080)
npm run dev

# app run (exec thin start) (localhost:8080)
npm run serve

# concat only
npm run prep

# build js/css for production
npm run build

# lint all *.js and *.vue files
npm run lint

# delete assets
npm run clean

# run unit tests
npm test
```

## TODO

### Widgets

+ Clock? DateTime?

+ Map? (with leaflet?)

+ Random image?
