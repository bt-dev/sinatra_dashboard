class WidgetYoutube < BaseWidget

  def self.resource_route
    '/watch'
  end

  def self.auth_required?
    false
  end

  def self.update_every
    nil
  end

  def data
    unless Settings.instance[:widget_youtube]
      Settings.instance[:widget_youtube] = []
    end
    ids = Settings.instance[:widget_youtube].to_json
    Settings.instance[:widget_youtube] = []
    [200, {"Content-Type" => "text/plain"}, [ids]]
  end

  def update
    id = params[:v]
    if Settings.instance[:widget_youtube].any?
      Settings.instance[:widget_youtube] << id
    else
      Settings.instance[:widget_youtube] = [id]
    end
  end

end
