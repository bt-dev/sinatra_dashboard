require 'yaml'

class BaseWidget

  def self.resource_route
    "/base"
  end

  def self.auth_required?
    false
  end

  def self.auth_providers
    []
  end

  def self.update_every
    nil
  end

  attr_accessor :request, :params

  def initialize(_req, _params)
    @request = _req
    @params = _params
  end

  def data #GET
    #provide widgets data
    "<h1>Hello world</h1>"
  end

  def update #POST
    # prepare / retrieve widget data
  end

  protected


  def redirect_to(path, message='', status=302)
    [ status, {'Content-Type' => 'text','Location' => path}, [message] ]
  end

end
