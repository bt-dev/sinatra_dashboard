/* eslint-disable no-unused-vars */

var _jquery = require( 'jquery' );
var $ = _jquery;
var _FW = require( 'freewall' );

var _vue = require( 'vue' );
// var _app = require( './App.vue' );

// <%= widgets.each { |widget| "var _#{widget} = require( '../../widgets/#{widget}/#{widget.camelize}.vue' );\n" } %>

var _widgetImage = require( '../../widgets/widget_image/WidgetImage.vue' );
var _widgetTable = require( '../../widgets/widget_table/WidgetTable.vue' );
var _widgetText  = require( '../../widgets/widget_text/WidgetText.vue' );
var _widgetFitbit  = require( '../../widgets/fitbit_widget/WidgetFitbit.vue' );
var _widgetWeather  = require( '../../widgets/widget_weather/WidgetWeather.vue' );
var _widgetChart  = require( '../../widgets/widget_chart/WidgetChart.vue' );
var _widgetMeme  = require( '../../widgets/widget_meme/WidgetMeme.vue' );
var _widgetChuck  = require( '../../widgets/widget_chuck/widget_chuck.vue' );
var _widgetYoutube = require( '../../widgets/widget_youtube/WidgetYoutube.vue' );

var UPDATE_TIME = 5000;  // ms

_vue.use( require( 'vue-resource' ) );

var vue = new _vue({
  el: 'body',
  //components: { App: _app },
  components: {
    WidgetImage: _widgetImage,
    WidgetTable: _widgetTable,
    WidgetText: _widgetText,
    WidgetWeather: _widgetWeather,
    WidgetFitbit: _widgetFitbit,
    WidgetChart: _widgetChart,
    WidgetMeme: _widgetMeme,
    WidgetChuck: _widgetChuck,
    WidgetYoutube: _widgetYoutube
  },
  ready: function() {
    var that = this;
    // this.$broadcast( 'init' );
    this.$broadcast( 'update' );
    window.setInterval( function() {
      console.log( '[UPDATE]' );
      that.$broadcast( 'update' );
    }, UPDATE_TIME );

    console.log( '[App READY]' );

    $(document).ready( function() {
      console.log( '[jQuery READY]' );

      var wall = new _FW.Freewall('.freewall');
      wall.reset({
        animate: true,
        draggable: true,
        selector: '.brick',
        cellW: 240,
        cellH: 240,
        // delay: 30,
        onResize: function() {
          // wall.refresh($(window).width() - 30, $(window).height() - 30);
          wall.refresh();
        }
      });
      // caculator width and height for IE7;
      // wall.fitZone($(window).width() - 30 , $(window).height() - 30);
      wall.fitWidth();

      // $(".gridster ul").gridster({
      //   widget_margins: [10, 10],
      //   widget_base_dimensions: [140, 140],
      //   shift_larger_widgets_down: false
      // });
    });
  }
});
