require 'byebug'
require './settings'

String.class_eval do
  def camelize
    self.split("_").collect do |m|
      m.capitalize
    end.join("")
  end

  def constantize
    Object.const_get self
  end
end

Hash.class_eval do
  def deep_symbolize_keys
    deep_transform_keys{ |key| key.to_sym rescue key }
  end

  protected

  def deep_transform_keys(&block)
    result = {}
    each do |key, value|
      result[yield(key)] = value.is_a?(Hash) ? value.deep_transform_keys(&block) : value
    end
    result
  end
end

module Utils
  require './widgets/base_widget'
  require 'rufus-scheduler'

  def configure_widgets!(app)

    app.class_eval do

      auth_providers = {}

      @scheduler = Rufus::Scheduler.new

      Dir.glob('./widgets/**/*.rb').each do |file|

        class_name = file.split("/").last.gsub(".rb","").camelize
        next if class_name.eql? 'BaseWidget'

        require file
        klass = class_name.constantize

        if (k = klass.new(nil,nil)).respond_to?(:data) && k.respond_to?(:update) && klass.respond_to?(:resource_route)

          p "Loading widget: #{klass} at route #{klass.resource_route}"
          p "> Setting up routes for #{klass}"
          #setting up data route
          get klass.resource_route.to_s do
            klass.new(request, params).data
          end

          #setting up update route
          post klass.resource_route.to_s do
            klass.new(request, params).update
          end

          if klass.auth_required?
            p "> Auth required for #{klass}"

            klass.auth_providers.each do |k|
              ">> Adding #{klass} to provider '#{k}' responders ..."
              auth_providers[k] ||= []
              auth_providers[k] << klass
            end
          end


          unless klass.update_every.nil?
            p "> Setting up update schedule for #{klass} every #{klass.update_every}"
            @scheduler.every klass.update_every do
              p "*** Updating data for #{klass}"
              klass.new(nil, nil).update
            end
          end

        else
          raise StandardError
        end

      end #end of Dir loop

      if auth_providers.keys.any? #at least one widget is using oAuth providers

        auth_providers.each do |k, classes|

          p "Setting up responders for provider #{k}: #{classes}"

          post "/auth/#{k}/callback" do
            Settings.instance["#{k}_auth_token"] = request.env['omniauth.auth']['credentials']['token']
            Settings.instance["#{k}_refresh_token"] = request.env['omniauth.auth']['credentials']['refresh_token']
            Settings.instance["#{k}_uid"] = request.env['omniauth.auth']['uid']

            classes.each do |controller|
              controller.new(request, params).update
            end
            [ 200, {'Content-Type' => 'text/html'}, ["Authentication completed, please close this tab"] ]
          end

          get "/auth/#{k}/callback" do
            Settings.instance["#{k}_auth_token"] = request.env['omniauth.auth']['credentials']['token']
            Settings.instance["#{k}_refresh_token"] = request.env['omniauth.auth']['credentials']['refresh_token']
            Settings.instance["#{k}_uid"] = request.env['omniauth.auth']['uid']

            classes.each do |controller|
              controller.new(request, params).update
            end
            [ 200, {'Content-Type' => 'text/html'}, ["Authentication completed, please close this tab"] ]
          end

          post "/auth/failure" do
            byebug
            [ 200, {'Content-Type' => 'text/html'}, ["Authentication failed, please try again later"] ]
          end

          get "/auth/failure" do
            byebug
            [ 200, {'Content-Type' => 'text/html'}, ["Authentication failed, please try again later"] ]
          end

        end #auth_providers end
      end

    end #end of class_eval

  end

end
