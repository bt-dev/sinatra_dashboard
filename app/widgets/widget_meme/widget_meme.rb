class WidgetMeme < BaseWidget

  def self.resource_route
    '/meme'
  end

  def self.auth_required?
    false
  end

  def self.update_every
    nil
  end

  def data #GET
    unless File.exists?(File.expand_path('.') + "/public/meme.jpg")
      update
    end
    [200, {"Content-Type" => "application/json"}, [{result: "/meme.jpg?v=#{Time.now.to_i}"}.to_json]]
  end

  def update
    #TODO: implement parameter handling

    base_url = 'https://ronreiter-meme-generator.p.mashape.com/meme'
    bottom_string     = !params[:bottom].nil? ? params[:bottom].to_s.gsub(' ', '+') : 'Test1'
    top_string        = !params[:top].nil? ? params[:top].to_s.gsub(' ', '+') : 'Test'
    meme_name         = (params[:meme].to_s.eql?("") ? 'Condescending Wonka' : params[:meme]).gsub(' ', '+')

    #X-Mashape-Key
    api_key = "lpoMdaa8NQmshGuRP4cKQzpKCKQep1W1RYijsnbKmFZBpopJ6D" # "IkjIWCBpnUmshF5hJWnJK2nTdvZRp1TGXNbjsnYXJ24Bdud3f2"

    url_string = base_url +'?bottom=' + bottom_string + '&top=' + top_string +'&font=Impact&&font_size=50' + '&meme=' + meme_name

    response = Unirest.get( url_string, headers: {"X-Mashape-Key" => api_key})
    if response.code.eql? 200
      File.write(File.expand_path('.') + '/public/meme.jpg', response.body)
    end

    [200, 'Ok']

  end

end
