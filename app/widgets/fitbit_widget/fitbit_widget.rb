class FitbitWidget < BaseWidget

  def self.resource_route
    '/fitbit'
  end

  def self.auth_required?
    true
  end

  def self.auth_providers
    [:fitbit]
  end

  def self.update_every
    '2m'
  end

  def data #GET

    if Settings.instance[:fitbit_auth_token].nil?
      [200, {"Content-Type" => "text/html"}, ["<a href='/auth/fitbit' target='_blank'>Autenticazione necessaria</a>"]]
    else
      [200, {"Content-Type" => "application/json"}, [{result: load_data}.to_json]]
    end

  end

  def update
    #TODO: implement parameter handling

    response = Unirest.get "https://api.fitbit.com/1/user/#{Settings.instance[:fitbit_uid]}/activities.json",
        headers: {
          'Authorization' => "Bearer #{Settings.instance[:fitbit_auth_token]}",
          'Content-Type' => 'application/json',
          'Accept' => 'application/json'
        }

    if response.code == 200
      store_data(response.body)
      [200, 'Ok']
    else
      [ 503, 'Service not available' ]
    end
  end

  protected

  def load_data
    @fitbit_data ||= (File.exists?(data_file_path) ? JSON.parse(File.read(data_file_path)) : {})
  end

  def store_data(data)
    @fitbit_data = data
    File.write(data_file_path, data.to_json)
  end

  def data_file_path
    File.expand_path('.') + "/data/fitbit_data.json"
  end

end
