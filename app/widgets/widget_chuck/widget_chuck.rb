class WidgetChuck < BaseWidget

  def self.resource_route
    '/chuck'
  end

  def self.auth_required?
    false
  end

  def self.update_every
    '2m'
  end

  def data
    unless Settings.instance[:widget_chuck]
      update
    end
    [200, {"Content-Type" => "application/json"}, [Settings.instance[:widget_chuck]]]
  end

  def update
    url = 'http://api.icndb.com/jokes/random?limitTo=[nerdy]&escape=javascript'
    response = Unirest.get(url)
    @res = response.body

    Settings.instance[:widget_chuck] = {result: @res}.to_json
  end
end
