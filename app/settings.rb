require 'singleton'
require 'yaml'

class Settings
  include Singleton

  def initialize(file_path='')
    @file_path = file_path unless file_path.eql? ''
    load!
  end

  def file_path
    @file_path ||= File.expand_path('.') + "/config/secrets.yml"
  end

  def data
    @data
  end

  def [](key)
    @data[key]
  end

  def []=(key, value)
    @data[key] = value
    store!
  end

  protected

  def load!
    @data ||= (File.exists?(file_path) ? YAML.load(File.read(file_path)).deep_symbolize_keys : {})
  end

  def store!
    File.write(file_path, @data.to_yaml)
  end
end